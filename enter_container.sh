#!/bin/sh

podman run -it --rm -e WAYLAND_DISPLAY=wayland-0 \
                    -e XDG_RUNTIME_DIR=/run/user/1000 \
                    -v /run/user/1000/pipewire-0:/run/user/1000/pipewire-0 \
                    -v /run/user/1000/wayland-0:/run/user/1000/wayland-0 \
                    -v /dev/dri:/dev/dri \
                    -v $PWD:/mnt \
                    pwdebian:latest /bin/bash
