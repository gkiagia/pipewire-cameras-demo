FROM debian:unstable

ENV LANG C.UTF-8

RUN apt-get update && apt-get install -y \
	gstreamer1.0-tools \
	gstreamer1.0-pipewire \
	gstreamer1.0-plugins-good \
	gstreamer1.0-plugins-base \
	gstreamer1.0-gl \
	pipewire-audio-client-libraries \
	pipewire \
	wireplumber \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*
