#!/bin/sh

gst-launch-1.0 \
    pipewiresrc stream-properties="p,node.target=v4l2:/dev/video0" name=c1 \
    pipewiresrc stream-properties="p,node.target=v4l2:/dev/video2" name=c2 \
    compositor name=cmp sink_1::ypos=480 \
    c1. ! video/x-raw,width=640,height=480 ! cmp.sink_0 \
    c2. ! video/x-raw,width=640,height=480,framerate=10/1 ! cmp.sink_1 \
    cmp. ! glimagesink
